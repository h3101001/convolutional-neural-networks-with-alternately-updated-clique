import pickle
import matplotlib.pyplot as plt
with open('D:/NCHU/1072/machine learning/HW/project2/CliqueNet-master/cifar-10-batches-py/test_batch', 'rb') as fo:
    dict = pickle.load(fo, encoding='latin1')
if 'data' in dict:
    dict['data'] = dict['data'].reshape((-1, 3, 32, 32)).swapaxes(1, 3).swapaxes(1, 2)
    data = dict['data']
plt.imshow(data[2])
plt.show()